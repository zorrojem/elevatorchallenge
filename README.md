# README #

How to run the elevator challenge

### Built using the following stack (older versions may work) ###

* node.js - v12.16.1
* npm - v6.13.4 
* VS Code


### How do I get set up? ###

* Clone repository to disk. [Source available on bitbucket](https://bitbucket.org/zorrojem/elevatorchallenge)
* Open cmd and navigate to location of cloned repository
* Run **npm install**
* Launch using **node index.js** command
* Open a browser e.g. Chrome and navigate to http://localhost:3000/

### Code By ###

(c) Jeremie Alexis

