var express = require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', function(socket){
  socket.on('lift event', function(evt){
    io.emit('lift event', evt);
  });

  socket.on('position event', function(evt){
    io.emit('position event', evt);
  });

  socket.on('request event', function(evt){
    io.emit('request event', evt);
  });

  socket.on('destination event', function(evt){
    io.emit('destination event', evt);
  });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
