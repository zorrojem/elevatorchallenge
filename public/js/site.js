var doorOpening;
var doorClosing;
var movingUp = false;
var movingDown = false;
var startTime, endTime;
var liftDirection = "up";
var liftLevel = 1;

function Elevator(id) {
    this.level = 1;
    this.direction = "up"; // (down, rest, up)
    this.time = 0;
    this.passengerCount = 0;
    this.levelSpacing = $(id).height();
    this.minLevel = $('#level-one').offset().top;
    this.maxLevel = $('#level-ten').offset().top;
    this.step = 10;
    this.liftOffset = $(id).offset().top;
    this.requests = [];
    this.destination = 0;

    this.level2 = $('#level-two').offset().top;
    this.level3 = $('#level-three').offset().top;
    this.level4 = $('#level-four').offset().top;
    this.level5 = $('#level-five').offset().top;
    this.level6 = $('#level-six').offset().top;
    this.level7 = $('#level-seven').offset().top;
    this.level8 = $('#level-eight').offset().top;
    this.level9 = $('#level-nine').offset().top;

    this.openDoors = function () {
        doorOpening = setInterval(function () { open(id); }, 300)
    }

    this.closeDoors = function () {
        doorClosing = setInterval(function () { close(id); }, 300)
    }

    this.moveUp = function (nextLevel) {
        this.closeDoors();
        var that = this;
        var liftOffset = this.liftOffset;
        var maxLevel = this.maxLevel;
        var step = this.step;

        var socket = io.connect();

        movingUp = setInterval(function () {
            if (liftOffset > maxLevel && (nextLevel && liftOffset > nextLevel)) {
                liftOffset -= step;
                $(id).offset({ top: liftOffset, left: 807 });
                liftOffset = $(id).offset().top;
                console.log('going this');
            } else {
                if (liftOffset = $(id).offset().top < maxLevel) {
                    $(id).offset({ top: maxLevel, left: 807 });
                    liftOffset = $(id).offset().top;
                }

                clearInterval(movingUp);
                that.clearDestination();
                that.liftDoorRoutine();
                console.log('going that');
                if(movingUp)
                    that.updateRequests(that.requests);

            }
            socket.emit('position event', that.calculateLevel(that.liftOffset, 'up'));
        }, 100);               
    }

    this.moveDown = function (nextLevel) {
        this.closeDoors();
        var that = this;
        var liftOffset = this.liftOffset;
        var minLevel = this.minLevel;
        var step = this.step;

        var socket = io.connect();

        movingDown = setInterval(function () {
            if (liftOffset < minLevel && (nextLevel && liftOffset < nextLevel)) {
                liftOffset += step;
                $(id).offset({ top: liftOffset, left: 807 });
                liftOffset = $(id).offset().top;

            } else {
                if (liftOffset = $(id).offset().top > minLevel) {
                    $(id).offset({ top: minLevel, left: 807 });
                    liftOffset = $(id).offset().top;
                }

                clearInterval(movingDown);
                that.clearDestination();
                that.liftDoorRoutine();

                if(movingDown)
                    that.updateRequests(that.requests);

            }
            socket.emit('position event', that.calculateLevel(that.liftOffset, 'down'));
        }, 100);
        
    }

    this.liftDoorRoutine = function () {
        this.liftOffset = $(id).offset().top;
        console.log(this.liftOffset);
        this.level = this.calculateLevel(this.liftOffset, 'up');
        liftLevel = this.level;

       // this.openDoors();
    }

    this.clearDestination = function () {
        if (this.requests.length > 0) {
            if(this.requests[0].type == 'request'){
                var elements = $(`i[data-level='${this.requests[0].level}']`);
                var item = $(elements).filter(`i[data-direction='${this.requests[0].direction}']`);
                $(item).css("background-color", "transparent");
            } else if(this.requests[0].type == 'destination') {
                var elements = $('.destination-btn');
                console.log(this.requests[0].destination);
                var item = $(elements).filter(`button[data-level=${this.requests[0].destination}]`);
                console.log(item);
                $(item).css("background-color", '#343a40');
            }            
            
            $('#requests li').first().remove();

            if (this.requests[0].type == "request") {
                this.passengerCount += 1;
                $('#passenger-count').text(this.passengerCount);
            }

            this.requests.shift();
        }

        endTime = new Date();
        var diff = endTime - startTime;
        $('#time-elapsed').text(diff /= 1000);
    }

    this.setLevel = function (level) {
        if (level == 1) {
            $(id).offset({ top: this.minLevel, left: 807 });
            this.liftOffset = this.minLevel;
        } else if(level == 2){
            $(id).offset({ top: this.level2, left: 807 });
            this.liftOffset = this.level2;
        }
        else {
            $(id).offset({ top: this.maxLevel, left: 807 });
            this.liftOffset = this.maxLevel;
        }
    }

    this.goToLevel = function (position) {
        console.log(this.liftOffset);
        startTime = new Date();
        this.liftOffset = $(id).offset().top;
        if (this.liftOffset > position) {
            clearInterval(movingUp);
            this.moveUp(position);
        }

        else if (this.liftOffset < position) {
            clearInterval(movingDown);
            this.moveDown(position);
        }

    }

    this.updatePos = function (pos) {
        this.level = pos.level;
        this.direction = pos.direction;

        $('#lift-direction').text(pos.direction);
        liftDirection = pos.direction;
        $('#current-level').text(pos.level);
        liftLevel = pos.level;
    }

    this.updateRequests = function (requests) {

        if (requests.length > 0) {
            requests.sort(destinationSort)
            this.destination = requests[0].level;
            $('#lift-destination').text(this.destination);
            this.goToLevel(this.calculatePosition(this.destination));
        }

        console.log(requests);
    }

    this.addRequest = function (request) {

        var exists = false;
        $.each(this.requests, function (index, value) {
            if (request.level == value.level && request.direction == value.direction)
                exists = true;
        });

        if (!exists) {
            this.requests.push(request);
            var socket = io.connect();
            socket.emit('request event', this.requests);
            $('#requests').append($('<li>').addClass('list-group-item').text('From: ' + request.level + ' Direction: ' + request.direction + " To: " + (request.destination ? request.destination : '?')));
        }
    }

    this.addDestination = function (request) {
        var exists = false;
        $.each(this.requests, function (index, value) {
            if (request.destination == value.level)
                exists = true;
        });

        if (!exists) {
            if (request.destination > liftLevel)
                request.direction = 'down';
            else if (request.destination > liftLevel)
                request.direction = 'up';

            request.level = request.destination;

            this.requests.push(request);
            var socket = io.connect();
            socket.emit('request event', this.requests);
            $('#requests').append($('<li>').addClass('list-group-item').text('From: ' + liftLevel + ' Direction: ' + request.direction + " To: " + (request.destination ? request.destination : '?')));
        }
    }

    this.getRequests = function () {
        return this.requests;
    }

    this.calculateLevel = function (position, direction) {
        if (position > this.maxLevel && position < this.level9)
            return { level: 9, direction: direction };
        else if (position > this.level9 && position < this.level8)
            return { level: 8, direction: direction };
        else if (position > this.level8 && position < this.level7)
            return { level: 7, direction: direction };
        else if (position > this.level7 && position < this.level6)
            return { level: 6, direction: direction };
        else if (position > this.level6 && position < this.level5)
            return { level: 5, direction: direction };
        else if (position > this.level5 && position < this.level4)
            return { level: 4, direction: direction };
        else if (position > this.level4 && position < this.level3)
            return { level: 3, direction: direction };
        else if (position > this.level3 && position < this.level2)
            return { level: 2, direction: direction };
        else if (position > this.level2 && (position < this.minLevel || position > this.minLevel))
            return { level: 1, direction: direction };
        else
            return { level: 10, direction: direction };
    }

    this.calculatePosition = function (level) {
        switch (level) {
            case 1:
                return this.minLevel;
                break;
            case 2:
                return this.level2;
                break;
            case 3:
                return this.level3;
                break;
            case 4:
                return this.level4;
                break;
            case 5:
                return this.level5;
                break;
            case 6:
                return this.level6;
                break;
            case 7:
                return this.level7;
                break;
            case 8:
                return this.level8;
                break;
            case 9:
                return this.level9;
                break;
            case 10:
                return this.maxLevel;
                break;
        }
    }
}

// this needs more work
function destinationSort(x, y) {

    var requestsFromSameLevel = x.level == y.level;
    var requestsLowerThanLift = x.level < liftLevel && y.level < liftLevel;
    var requestsHigherThanLift = x.level > liftLevel && y.level > liftLevel;
    var requestsInSameDirection = x.direction == y.direction;
    var requestsInLiftDirection = x.direction == liftDirection && y.direction == liftDirection
        || (liftDirection == "up" && x.level == 10 || y.level === 10)
        || (liftDirection == "down" && x.level == 1 || y.level === 1);
    var requestsInOppositeLiftDirection = x.direction != liftDirection && y.direction != liftDirection;
    var requestXLowerThanRequestY = x.level < y.level;
    var requestXHigherThanRequestY = x.level > y.level;
    var xInLiftDirectionYOpposite = x.direction == liftDirection && y.direction != liftDirection;
    var yInLiftDirectionXOpposite = x.direction != liftDirection && y.direction == liftDirection;
    var requestXLowerThanLiftAndYHigher = x.level < liftLevel && y.level > liftLevel;
    var requestYLowerThanLiftAndXHigher = x.level > liftLevel && y.level < liftLevel;

if(liftDirection == "up"){
    if (requestsHigherThanLift && requestsInSameDirection && requestsInLiftDirection && requestXLowerThanRequestY)
    return 1;
if (requestsHigherThanLift && requestsInSameDirection && requestsInLiftDirection && requestXHigherThanRequestY)
    return 1;
if (requestsHigherThanLift && requestsFromSameLevel && xInLiftDirectionYOpposite)
    return -1;
if (requestsHigherThanLift && requestsFromSameLevel && yInLiftDirectionXOpposite)
    return 1;
if (requestsHigherThanLift && xInLiftDirectionYOpposite)
    return -1;
if (requestsHigherThanLift && yInLiftDirectionXOpposite)
    return 1;
if (requestsHigherThanLift && requestsInOppositeLiftDirection && requestXHigherThanRequestY)
    return -1;
if (requestsHigherThanLift && requestsInOppositeLiftDirection && requestXLowerThanRequestY)
    return 1;

if (requestsLowerThanLift && requestsInSameDirection && requestsInLiftDirection && requestXLowerThanRequestY)
    return 1;
if (requestsLowerThanLift && requestsInSameDirection && requestsInLiftDirection && requestXHigherThanRequestY)
    return 1;
if (requestsLowerThanLift && requestsFromSameLevel && xInLiftDirectionYOpposite)
    return -1;
if (requestsLowerThanLift && requestsFromSameLevel && yInLiftDirectionXOpposite)
    return 1;
if (requestsLowerThanLift && xInLiftDirectionYOpposite)
    return -1;
if (requestsLowerThanLift && yInLiftDirectionXOpposite)
    return 1;

if (requestXLowerThanLiftAndYHigher && requestsInSameDirection && requestsInLiftDirection)
    return 1;
if (requestXLowerThanLiftAndYHigher && xInLiftDirectionYOpposite)
    return -1;
if (requestXLowerThanLiftAndYHigher && yInLiftDirectionXOpposite)
    return 1;
if (requestYLowerThanLiftAndXHigher && requestsInSameDirection && requestsInLiftDirection)
    return -1;
if (requestYLowerThanLiftAndXHigher && xInLiftDirectionYOpposite)
    return -1;
if (requestYLowerThanLiftAndXHigher && yInLiftDirectionXOpposite)
    return 1;
}
else if (liftDirection == "down"){
    if (requestsHigherThanLift && requestsInSameDirection && requestsInLiftDirection && requestXLowerThanRequestY)
        return -1;
    if (requestsHigherThanLift && requestsInSameDirection && requestsInLiftDirection && requestXHigherThanRequestY)
        return -1;
    if (requestsHigherThanLift && requestsFromSameLevel && xInLiftDirectionYOpposite)
        return 1;
    if (requestsHigherThanLift && requestsFromSameLevel && yInLiftDirectionXOpposite)
        return -1;
    if (requestsHigherThanLift && xInLiftDirectionYOpposite)
        return 1;
    if (requestsHigherThanLift && yInLiftDirectionXOpposite)
        return -1;
    if (requestsHigherThanLift && requestsInOppositeLiftDirection && requestXHigherThanRequestY)
        return 1;
    if (requestsHigherThanLift && requestsInOppositeLiftDirection && requestXLowerThanRequestY)
        return -1;

    if (requestsLowerThanLift && requestsInSameDirection && requestsInLiftDirection && requestXLowerThanRequestY)
        return -1;
    if (requestsLowerThanLift && requestsInSameDirection && requestsInLiftDirection && requestXHigherThanRequestY)
        return -1;
    if (requestsLowerThanLift && requestsFromSameLevel && xInLiftDirectionYOpposite)
        return 1;
    if (requestsLowerThanLift && requestsFromSameLevel && yInLiftDirectionXOpposite)
        return -1;
    if (requestsLowerThanLift && xInLiftDirectionYOpposite)
        return 1;
    if (requestsLowerThanLift && yInLiftDirectionXOpposite)
        return -1;

    if (requestXLowerThanLiftAndYHigher && requestsInSameDirection && requestsInLiftDirection)
        return -1;
    if (requestXLowerThanLiftAndYHigher && xInLiftDirectionYOpposite)
        return 1;
    if (requestXLowerThanLiftAndYHigher && yInLiftDirectionXOpposite)
        return -1;
    if (requestYLowerThanLiftAndXHigher && requestsInSameDirection && requestsInLiftDirection)
        return 1;
    if (requestYLowerThanLiftAndXHigher && xInLiftDirectionYOpposite)
        return 1;
    if (requestYLowerThanLiftAndXHigher && yInLiftDirectionXOpposite)
        return -1;
} 

    return 0;
}

function open(id) {
    var src = $(id).attr('src');
    if (src === 'images/lift-doors-closed.png')
        $(id).attr('src', 'images/lift-third-open.png');
    else if (src === 'images/lift-third-open.png')
        $(id).attr('src', 'images/lift-half-open.png');
    else if (src === 'images/lift-half-open.png')
        $(id).attr('src', 'images/lift-full-open.png');
    else
        clearInterval(doorOpening);
}

function close(id) {
    var src = $(id).attr('src');
    if (src === 'images/lift-full-open.png')
        $(id).attr('src', 'images/lift-half-open.png');
    else if (src === 'images/lift-half-open.png')
        $(id).attr('src', 'images/lift-third-open.png');
    else if (src === 'images/lift-third-open.png')
        $(id).attr('src', 'images/lift-doors-closed.png');
    else
        clearInterval(doorClosing);
}
